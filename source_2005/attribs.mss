@Part(attribs, Root="ada.mss")

@Comment{$Date: 2000/08/08 22:56:19 $}
@LabeledInformativeAnnex{Language-Defined Attributes}

@comment{$Source: e:\\cvsroot/ARM/Source/attribs.mss,v $}
@comment{$Revision: 1.14 $}

@begin{Intro}
@Defn{attribute}This annex summarizes the definitions given elsewhere
of the language-defined attributes.

@AttributeList
@end{Intro}