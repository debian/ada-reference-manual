@Part(title, Root="ada.mss")

@comment{$Source: e:\\cvsroot/ARM/Source/title.mss,v $}
@comment{$Revision: 1.33 $ $Date: 2006/11/09 06:29:49 $}

@begin{ISOOnly}
@Noparanum@Right{@Swiss{@Grow{@B{INTERNATIONAL STANDARD} ISO/IEC 8652:@Chg{Version=[2],New=[2007(E) Ed. 3],Old=[1995(E)@Chg{Version=[1], New=[ with COR.1:2001], Old=[]}]}}}}
@end{ISOOnly}
@begin{NotISO}
@Noparanum@Right{@Swiss{@Grow{@B{Ada Reference Manual}, ISO/IEC 8652:@Chg{Version=[2],New=[2007(E) Ed. 3],Old=[1995(E)@Chg{Version=[1], New=[ with COR.1:2001], Old=[]}]}}}}
@end{NotISO}

@Noparanum@ @*
@ @*
@ @*

@begin{ISOOnly}
@Noparanum@swiss{@shrink{@shrink{@shrink{INTERNATIONAL ORGANIZATION FOR STANDARDIZATION}}}}

@Noparanum@swiss{@shrink{@shrink{@shrink{INTERNATIONAL ELECTROTECHNICAL COMMISSION}}}}
@end{ISOOnly}

@Noparanum@ @*
@ @*

@begin{ISOOnly}
@Noparanum@Swiss{@Grow{@Grow{@Grow{@Grow{@Grow{@b{Information technology @Em Programming languages @Em Ada}}}}}}}
@end{ISOOnly}

@Noparanum@ @;@comment{A dummy paragraph containing just a blank}

@begin{NotISO}
@Noparanum@ @*@;@comment{A dummy paragraph containing three blank lines}
@ @*
@ @*
@ @*

@end{NotISO}

@begin{ISOOnly}
@Comment{Jim Moore wants this deleted, as it is confusing.}
@Noparanum@Chg{New=[@ @*@Comment{Dummy paragraph}],
               Old=[@Swiss{[Revision of first edition (ISO 8652:1987)]}]}
@end{ISOOnly}

@Noparanum@ @*

@begin{NotISO}
@begin{RMOnly}
@Noparanum@Heading{@Grow{@Grow{@Grow{Ada Reference Manual}}}}
@end{RMOnly}
@begin{AARMOnly}
@Noparanum@Heading{@Grow{@Grow{@Grow{Annotated Ada Reference Manual}}}}
@end{AARMOnly}

@Noparanum@ @;@comment{A dummy paragraph containing just a blank}

@Noparanum@Center{@Swiss{@Grow{ISO/IEC 8652:1995(E)}}}
@Noparanum@Center{@Swiss{@Chg{Version=[1], New=[@Grow{with Technical Corrigendum 1}], Old=[]}}}
@Noparanum@Center{@Swiss{@Chg{Version=[2], New=[@Grow{and Amendment 1}], Old=[]}}}
@end{NotISO}

@Noparanum@ @;@comment{A dummy paragraph containing just a blank}

@Noparanum@Center{@Swiss{@Grow{Language and Standard Libraries}}}

@Noparanum@Comment{The following puts the copyright near the bottom of the page}
@ @*@*@*@*@*@*@*@*@*@*

@Noparanum@;Copyright @Latin1(169) 1992, 1993, 1994, 1995  Intermetrics, Inc.

@Noparanum@;@Chg{Version=[1], New=[Copyright @Latin1(169) 2000  The MITRE Corporation, Inc.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[Copyright @Latin1(169) 2004, 2005, 2006  AXE Consultants], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[Copyright @Latin1(169) 2004, 2005, 2006  Ada-Europe], Old=[]}

@NewPage
@Comment{For ISO version, the back of the title page is blank, and the
copyright ought to appear at the bottom of the table of contents. [But that
probably is changed by now.}
@begin{NotISO}
@Comment{The following puts the copyright near the bottom of the page}
@Noparanum@ @*@*@*@*@*

@Noparanum@;@b{Ada Reference Manual - Language and Standard Libraries}

@Noparanum@;Copyright @Latin1(169) 1992, 1993, 1994, 1995, Intermetrics, Inc.

@Noparanum@;This copyright is assigned to the U.S. Government.  All rights reserved.

@Noparanum@;This document may be copied, in whole or in part, in any form or by any means,
as is or with alterations, provided that (1) alterations are clearly marked as
alterations and (2) this copyright notice is included unmodified in any copy.
Compiled copies of standard library units and examples need not contain this
copyright notice so long as the notice is included in all copies of source code
and documentation.

@ThinLine

@Noparanum@;@ @;@comment{A dummy paragraph containing just a blank}

@Noparanum@;@Chg{Version=[1], New=[@b{Technical Corrigendum 1}], Old=[]}

@Noparanum@;@Chg{Version=[1], New=[Copyright @Latin1(169) 2000, The MITRE Corporation.  All Rights Reserved.], Old=[]}

@Noparanum@;@Chg{Version=[1], New=[This document may be copied, in whole or in part, in any form or by any means,
as is, or with alterations, provided that (1) alterations are clearly marked as
alterations and (2) this copyright notice is included unmodified in any copy.
Any other use or distribution of this document is prohibited without the prior
express permission of MITRE.], Old=[]}

@Noparanum@;@Chg{Version=[1], New=[You use this document on the condition that
you indemnify and hold harmless MITRE, its Board of Trustees, officers, agents,
and employees, from any and all liability or damages to yourself or your
hardware or software, or third parties, including attorneys' fees, court costs,
and other related costs and expenses, arising out of your use of this document
irrespective of the cause of said liability.], Old=[]}

@Noparanum@;@Chg{Version=[1], New=[MITRE MAKES THIS DOCUMENT AVAILABLE ON AN "AS IS" BASIS AND MAKES NO WARRANTY,
EXPRESS OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY MERCHANTABILITY,
OR FUNCTIONING OF THIS DOCUMENT.  IN NO EVENT WILL MITRE BE LIABLE FOR ANY
GENERAL, CONSEQUENTIAL, INDIRECT, INCIDENTAL, EXEMPLARY, OR SPECIAL DAMAGES,
EVEN IF MITRE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.], Old=[]}

@Noparanum@;@ @;@comment{A dummy paragraph containing just a blank}

@Noparanum@;@Chg{Version=[2], New=[@b{Amendment 1}], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[Copyright @Latin1(169) 2004, 2005, 2006, AXE Consultants.  All Rights Reserved.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[This document may be copied, in whole or in part, in any form or by any means,
as is, or with alterations, provided that (1) alterations are clearly marked as
alterations and (2) this copyright notice is included unmodified in any copy.
Any other use or distribution of this document is prohibited
without the prior express permission of AXE.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[You use this document on the condition that
you indemnify and hold harmless AXE, its board, officers, agents, and
employees, from any and all liability or damages to yourself or your hardware
or software, or third parties, including attorneys' fees, court costs, and
other related costs and expenses, arising out of your use of this document
irrespective of the cause of said liability.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[AXE MAKES THIS DOCUMENT AVAILABLE ON AN "AS IS" BASIS AND MAKES NO WARRANTY,
EXPRESS OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY MERCHANTABILITY,
OR FUNCTIONING OF THIS DOCUMENT. IN NO EVENT WILL AXE BE LIABLE FOR ANY
GENERAL, CONSEQUENTIAL, INDIRECT, INCIDENTAL, EXEMPLARY, OR SPECIAL DAMAGES,
EVEN IF AXE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.], Old=[]}

@Noparanum@;@ @;@comment{A dummy paragraph containing just a blank}

@Noparanum@;@Chg{Version=[2], New=[@b{Consolidated Standard}], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[Copyright @Latin1(169) 2004, 2005, 2006, Ada-Europe.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[This document may be copied, in whole or in
part, in any form or by any means, as is, or with alterations, provided that
(1) alterations are clearly marked as alterations and (2) this copyright notice
is included unmodified in any copy. Any other use or distribution of this
document is prohibited without the prior express permission of Ada-Europe.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[You use this document on the condition that
you indemnify and hold harmless Ada-Europe and its Board from any and all
liability or damages to yourself or your hardware or software, or third
parties, including attorneys' fees, court costs, and other related costs and
expenses, arising out of your use of this document irrespective of the cause of
said liability.], Old=[]}

@Noparanum@;@Chg{Version=[2], New=[ADA-EUROPE MAKES THIS DOCUMENT AVAILABLE ON AN "AS IS" BASIS AND MAKES NO WARRANTY,
EXPRESS OR IMPLIED, AS TO THE ACCURACY, CAPABILITY, EFFICIENCY MERCHANTABILITY,
OR FUNCTIONING OF THIS DOCUMENT. IN NO EVENT WILL ADA-EUROPE BE LIABLE FOR ANY
GENERAL, CONSEQUENTIAL, INDIRECT, INCIDENTAL, EXEMPLARY, OR SPECIAL DAMAGES,
EVEN IF ADA-EUROPE HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.], Old=[]}
@end{NotISO}

